package com.progmatic.countriesandcities.daos;
import com.progmatic.countriesandcities.dtos.CountryDto;
import com.progmatic.countriesandcities.entities.Country;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Id;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CountryAutoDao extends JpaRepository<Country, String>{
    
    @Query(value = "select * from country" , nativeQuery = true)
    @Override
    ArrayList<Country> findAll();
    
     Country findByIso(String iso);
}
