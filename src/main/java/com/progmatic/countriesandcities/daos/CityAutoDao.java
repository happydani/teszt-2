/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.countriesandcities.daos;

import com.progmatic.countriesandcities.entities.City;
import java.util.ArrayList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author peti
 */
public interface CityAutoDao extends JpaRepository<City, String>{
    
    @Query(value = "select * from city" , nativeQuery = true)
    @Override
    ArrayList<City> findAll();
}
